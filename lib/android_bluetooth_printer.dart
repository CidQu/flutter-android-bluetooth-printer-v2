import 'dart:async';

import 'package:flutter/services.dart';

class AndroidBluetoothPrinter {
  static const MethodChannel _channel =
      const MethodChannel('android_bluetooth_printer_v2');

  /// Print to the first connected bluetooth printer
  static Future<String?> print(
    String text, {
    int? width,
    String? ipAddress,
    int? dotsFeedPaper,
  }) async {
    final String? version = await _channel.invokeMethod('print', {
      "text": text,
      if (ipAddress != null) "ip_address": ipAddress,
      if (width != null) "width": width,
      if (dotsFeedPaper != null) "dotsFeedPaper": dotsFeedPaper,
    });
    return version;
  }
}
